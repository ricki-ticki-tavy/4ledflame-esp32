//
// Created by dsporykhin on 01.03.20.
//

#include "TrembleFlameEffect.h"
#include "Defines.h"
#include "Logger.h"
#include "../../../../../.platformio/packages/framework-arduinoespressif32/cores/esp32/Arduino.h"

TrembleFlameEffect::TrembleFlameEffect(EFlame *eflame) : AbstractFlameEffect(eflame, true) {

}

void TrembleFlameEffect::applyEffect() {
    // выбрать направление смещения. Random работает криво - последнее значение не выпадает никогда
    int direction = random(0, 8);

    // подберем скорость изменений
    flame->stepCountToDo = flame->doubleRandom() * (double) (
            flame->getSettingsManager()->getSettings()->activeProfile.trembleSettings.maxStepCount
            - flame->getSettingsManager()->getSettings()->activeProfile.trembleSettings.minStepCount) +
                           flame->getSettingsManager()->getSettings()->activeProfile.trembleSettings.minStepCount;

    // вычислим величину изменения
    double newBrightDelta = (flame->doubleRandom() *
                                 (flame->getSettingsManager()->getSettings()->activeProfile.trembleSettings.maxBrightnessChangePrct
                                  -
                                  flame->getSettingsManager()->getSettings()->activeProfile.trembleSettings.minBrightnessChangePrct)
                                 +
                                 flame->getSettingsManager()->getSettings()->activeProfile.trembleSettings.minBrightnessChangePrct)
                                * (double)PWM_MAX_WIDTH
                                / (double) 100;

    // расчитаем индивидуальные делты
    for (int index = 0; index < 4; index++) {
        flame->brightDelta[index] =
                (flame->getAbsCurrentNormalBright() + newBrightDelta * flame->getSettingsManager()->getSettings()->activeProfile.trembleSettings.matrix[direction][index] - flame->brights[index]) /
                (double) flame->stepCountToDo;
    }
}

float TrembleFlameEffect::getProbabilityCoef(){
    return flame->getSettingsManager()->getSettings()->activeProfile.trembleSettings.probabilityCoef;
}
