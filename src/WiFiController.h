//
// Created by dsporykhin on 19.04.20.
//

//#include "WebServerController.h"
#include "SettingsManager.h"
#include "../../../../../.platformio/packages/framework-arduinoespressif32/cores/esp32/IPAddress.h"
#include "WebServerController.h"
#include "../../../../../.platformio/packages/framework-arduinoespressif32/libraries/WiFi/src/WiFiType.h"
#include "../../../../../.platformio/packages/framework-arduinoespressif32/libraries/WiFi/src/WiFiGeneric.h"

#ifndef EFLAMEESP8266_WIFICONTROLLER_H
#define EFLAMEESP8266_WIFICONTROLLER_H


class WiFiController {
private:
    static bool needReconnect;
    static bool reconnectorActive;
    static SettingsManager* settingsManager;
    static bool wasSuccedConnected;
    WebServerController* serverController;

    static void setApMode(GlobalSettings *settings, IPAddress *ipAddress, bool defaultApParam);
    static void tryToConnectToRouter();

    static void onWiFiEvent(WiFiEvent_t event);

    void init();
public:
    WiFiController(SettingsManager* settingsManager);

    void handle();
};

extern WiFiController* wiFiController;
#endif //EFLAMEESP8266_WIFICONTROLLER_H
