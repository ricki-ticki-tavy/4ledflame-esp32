//
// Created by dsporykhin on 29.02.20.
//

#ifndef EFLAME328_EFLAME_H
#define EFLAME328_EFLAME_H


#include "AbstractFlameEffect.h"
#include "SettingsManager.h"
#include "../../../../../.platformio/packages/framework-arduinoespressif32/variants/esp32/pins_arduino.h"

// кол-во форм, поддерживаемых менеджером
#define MAX_EFFECTS_COUNT 10

class AbstractFlameEffect;

class EFlame {
private:
    SettingsManager *settingsManager;

    long shutdownTimerStartedAt;
    long shutdownTimerOrigin;
    bool shutdownTimerActive;
    bool shutdownInProgress;

    // Нормальная средняя яркость
    double centerBrightnessPrct;

    double priorCenterBrightnessPrctInSettings;

    float getTotalEffectsProbability();

public:
    int ledMatrix[4] = {
            14, 26, 33, 18
    };

    // видеоэффекты
    AbstractFlameEffect *effects[MAX_EFFECTS_COUNT];

    // кол-во эффектов
    int effectCounter;

    // кол-во циклов до окончания действия эффекта
    int stepCountToDo;

    // массив дельт для яркостей 4-х светодиодов
    double brightDelta[4];

    // массив текущих яркостей
    double brights[4];

    // рандом от 0 до 1
    double doubleRandom();

    // абсолютное значение
    double doubleAbs(double src);

    // Добавление эффекта
    void appendEffect(AbstractFlameEffect *effect);

    // Рабочий цикл программы
    void loop();

    /**
     * Возвращает текущую НОРМАЛЬНУЮ яркость свечи. В процентах
     * @return
     */
    double getCurrentNormalPrightPrct();

    void setCurrentNormalPrightPrct(double value);

    /**
     * Возвращает текущую НОРМАЛЬНУЮ яркость свечи. в единицах PWM
     * @return
     */
    double getAbsCurrentNormalBright();

    EFlame(SettingsManager *settingsManager);

    SettingsManager *getSettingsManager();

private:

    // Установить яркость свеения светодиода
    void hdSetLedBright(int index);

    void applyNewEffect();

};


#endif //EFLAME328_EFLAME_H
