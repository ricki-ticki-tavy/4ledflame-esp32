//
// Created by dsporykhin on 24.04.20.
//

#include "DefaultProfilesInitializer.h"
#include "Defines.h"
#include "../../../../../.platformio/packages/framework-arduinoespressif32/cores/esp32/WString.h"

void DefaultProfilesInitializer::initSystemProfiles(Profile *userProfiles) {
    for (int i = 0; i < MAX_PROFILE_COUNTER; userProfiles[i++].isFree = true);

    float matrix[32] = {2.00, 2.00, -3.00, -3.00, -1.50, 2.00, -1.50, -3.00, -3.00, 2.00, 2.00, -3.00, -3.00, -1.50,
                        2.00, -1.50, -3.00, -3.00, 2.00, 2.00, -1.50, -3.00, -1.50, 2.00, 2.00, -3.00, -3.00, 2.00,
                        2.00, -1.50, -3.00, -1.50};


    userProfiles[0].isFree = false;
    userProfiles[0].system = true;
    String("Спокойная неяркая свеча").toCharArray(&userProfiles[0].name[0], sizeof(userProfiles[0].name), 0);

    userProfiles[0].flameSettings.brightnessCoefficientPrct = 100;
    userProfiles[0].flameSettings.loopIntervalMs = 20;
    userProfiles[0].flameSettings.centerBrightnessPrct = 35;
    userProfiles[0].flameSettings.newEffectProbabilityPer100CyclesPrct = 100;
    userProfiles[0].flameSettings.probabilityChangeActionPrct = 0.03;
    userProfiles[0].flameSettings.shutdownAfterSec = 0;

    userProfiles[0].normalizeSettings.probabilityCoef = 0.5;
    userProfiles[0].normalizeSettings.minStepCount = 10;
    userProfiles[0].normalizeSettings.maxStepCount = 50;

    userProfiles[0].hotAgeSettings.probabilityCoef = 0;
    userProfiles[0].hotAgeSettings.minBrightnessChangePrct = 15;
    userProfiles[0].hotAgeSettings.maxBrightnessChangePrct = 25;
    userProfiles[0].hotAgeSettings.minStepCount = 7;
    userProfiles[0].hotAgeSettings.maxStepCount = 25;

    userProfiles[0].avgBrightSweepSettings.probabilityCoef = 0.08;
    userProfiles[0].avgBrightSweepSettings.maxFadeDownPrct = 5;
    userProfiles[0].avgBrightSweepSettings.maxFadeUpPrct = 13;

    userProfiles[0].trembleSettings.probabilityCoef = 0.4;
    userProfiles[0].trembleSettings.minBrightnessChangePrct = 25;
    userProfiles[0].trembleSettings.maxBrightnessChangePrct = 40;
    userProfiles[0].trembleSettings.minStepCount = 30;
    userProfiles[0].trembleSettings.maxStepCount = 100;

    memcpy(&userProfiles[0].trembleSettings.matrix[0][0], &matrix[0], sizeof(matrix));


    userProfiles[1].isFree = false;
    userProfiles[1].system = true;
    String("Спокойная яркая свеча").toCharArray(&userProfiles[1].name[0], sizeof(userProfiles[1].name), 0);

    userProfiles[1].flameSettings.brightnessCoefficientPrct = 100;
    userProfiles[1].flameSettings.loopIntervalMs = 20;
    userProfiles[1].flameSettings.centerBrightnessPrct = 90;
    userProfiles[1].flameSettings.newEffectProbabilityPer100CyclesPrct = 100;
    userProfiles[1].flameSettings.probabilityChangeActionPrct = 0.03;
    userProfiles[1].flameSettings.shutdownAfterSec = 0;

    userProfiles[1].normalizeSettings.probabilityCoef = 0.5;
    userProfiles[1].normalizeSettings.minStepCount = 10;
    userProfiles[1].normalizeSettings.maxStepCount = 50;

    userProfiles[1].hotAgeSettings.probabilityCoef = 0;
    userProfiles[1].hotAgeSettings.minBrightnessChangePrct = 15;
    userProfiles[1].hotAgeSettings.maxBrightnessChangePrct = 25;
    userProfiles[1].hotAgeSettings.minStepCount = 7;
    userProfiles[1].hotAgeSettings.maxStepCount = 25;

    userProfiles[1].avgBrightSweepSettings.probabilityCoef = 0.08;
    userProfiles[1].avgBrightSweepSettings.maxFadeDownPrct = 5;
    userProfiles[1].avgBrightSweepSettings.maxFadeUpPrct = 13;

    userProfiles[1].trembleSettings.probabilityCoef = 0.4;
    userProfiles[1].trembleSettings.minBrightnessChangePrct = 25;
    userProfiles[1].trembleSettings.maxBrightnessChangePrct = 40;
    userProfiles[1].trembleSettings.minStepCount = 30;
    userProfiles[1].trembleSettings.maxStepCount = 100;

    memcpy(&userProfiles[1].trembleSettings.matrix[0][0], &matrix[0], sizeof(matrix));

    userProfiles[2].isFree = false;
    userProfiles[2].system = true;
    String("Гаснущий фитиль").toCharArray(&userProfiles[2].name[0], sizeof(userProfiles[2].name), 0);

    userProfiles[2].flameSettings.brightnessCoefficientPrct = 30;
    userProfiles[2].flameSettings.loopIntervalMs = 20;
    userProfiles[2].flameSettings.centerBrightnessPrct = 5;
    userProfiles[2].flameSettings.newEffectProbabilityPer100CyclesPrct = 100;
    userProfiles[2].flameSettings.probabilityChangeActionPrct = 0.2;
    userProfiles[2].flameSettings.shutdownAfterSec = 0;

    userProfiles[2].normalizeSettings.probabilityCoef = 0.5;
    userProfiles[2].normalizeSettings.minStepCount = 10;
    userProfiles[2].normalizeSettings.maxStepCount = 50;

    userProfiles[2].hotAgeSettings.probabilityCoef = 0.05;
    userProfiles[2].hotAgeSettings.minBrightnessChangePrct = 15;
    userProfiles[2].hotAgeSettings.maxBrightnessChangePrct = 20;
    userProfiles[2].hotAgeSettings.minStepCount = 10;
    userProfiles[2].hotAgeSettings.maxStepCount = 50;

    userProfiles[2].avgBrightSweepSettings.probabilityCoef = 0.08;
    userProfiles[2].avgBrightSweepSettings.maxFadeDownPrct = 5;
    userProfiles[2].avgBrightSweepSettings.maxFadeUpPrct = 13;

    userProfiles[2].trembleSettings.probabilityCoef = 1.4;
    userProfiles[2].trembleSettings.minBrightnessChangePrct = 5;
    userProfiles[2].trembleSettings.maxBrightnessChangePrct = 15;
    userProfiles[2].trembleSettings.minStepCount = 30;
    userProfiles[2].trembleSettings.maxStepCount = 100;

    memcpy(&userProfiles[2].trembleSettings.matrix[0][0], &matrix[0], sizeof(matrix));

    userProfiles[3].isFree = false;
    userProfiles[3].system = true;
    String("Свеча на ветру").toCharArray(&userProfiles[3].name[0], sizeof(userProfiles[3].name), 0);

    userProfiles[3].flameSettings.brightnessCoefficientPrct = 100;
    userProfiles[3].flameSettings.loopIntervalMs = 2;
    userProfiles[3].flameSettings.centerBrightnessPrct = 25;
    userProfiles[3].flameSettings.newEffectProbabilityPer100CyclesPrct = 100;
    userProfiles[3].flameSettings.probabilityChangeActionPrct = 0.03;
    userProfiles[3].flameSettings.shutdownAfterSec = 0;

    userProfiles[3].normalizeSettings.probabilityCoef = 0.3;
    userProfiles[3].normalizeSettings.minStepCount = 5;
    userProfiles[3].normalizeSettings.maxStepCount = 40;

    userProfiles[3].hotAgeSettings.probabilityCoef = 0;
    userProfiles[3].hotAgeSettings.minBrightnessChangePrct = 60;
    userProfiles[3].hotAgeSettings.maxBrightnessChangePrct = 70;
    userProfiles[3].hotAgeSettings.minStepCount = 1;
    userProfiles[3].hotAgeSettings.maxStepCount = 1;

    userProfiles[3].avgBrightSweepSettings.probabilityCoef = 0.18;
    userProfiles[3].avgBrightSweepSettings.maxFadeDownPrct = 4;
    userProfiles[3].avgBrightSweepSettings.maxFadeUpPrct = 15;

    userProfiles[3].trembleSettings.probabilityCoef = 0.9;
    userProfiles[3].trembleSettings.minBrightnessChangePrct = 15;
    userProfiles[3].trembleSettings.maxBrightnessChangePrct = 40;
    userProfiles[3].trembleSettings.minStepCount = 50;
    userProfiles[3].trembleSettings.maxStepCount = 200;

    memcpy(&userProfiles[3].trembleSettings.matrix[0][0], &matrix[0], sizeof(matrix));

    userProfiles[4].isFree = false;
    userProfiles[4].system = true;
    String("Свеча в ураган").toCharArray(&userProfiles[4].name[0], sizeof(userProfiles[4].name), 0);

    userProfiles[4].flameSettings.brightnessCoefficientPrct = 100;
    userProfiles[4].flameSettings.loopIntervalMs = 1;
    userProfiles[4].flameSettings.centerBrightnessPrct = 65;
    userProfiles[4].flameSettings.newEffectProbabilityPer100CyclesPrct = 100;
    userProfiles[4].flameSettings.probabilityChangeActionPrct = 0.03;
    userProfiles[4].flameSettings.shutdownAfterSec = 0;

    userProfiles[4].normalizeSettings.probabilityCoef = 0.3;
    userProfiles[4].normalizeSettings.minStepCount = 20;
    userProfiles[4].normalizeSettings.maxStepCount = 40;

    userProfiles[4].hotAgeSettings.probabilityCoef = 0.4;
    userProfiles[4].hotAgeSettings.minBrightnessChangePrct = 60;
    userProfiles[4].hotAgeSettings.maxBrightnessChangePrct = 70;
    userProfiles[4].hotAgeSettings.minStepCount = 1;
    userProfiles[4].hotAgeSettings.maxStepCount = 3;

    userProfiles[4].avgBrightSweepSettings.probabilityCoef = 0.18;
    userProfiles[4].avgBrightSweepSettings.maxFadeDownPrct = 40;
    userProfiles[4].avgBrightSweepSettings.maxFadeUpPrct = 50;

    userProfiles[4].trembleSettings.probabilityCoef = 0.7;
    userProfiles[4].trembleSettings.minBrightnessChangePrct = 5;
    userProfiles[4].trembleSettings.maxBrightnessChangePrct = 40;
    userProfiles[4].trembleSettings.minStepCount = 30;
    userProfiles[4].trembleSettings.maxStepCount = 120;

    memcpy(&userProfiles[4].trembleSettings.matrix[0][0], &matrix[0], sizeof(matrix));


}