//
// Created by dsporykhin on 29.02.20.
//

#ifndef EFLAME328_FLAMEEFFECT_H
#define EFLAME328_FLAMEEFFECT_H


#include "EFlame.h"

class EFlame;

class AbstractFlameEffect {
public:
    // Отключение дельт предыдущих эффектов
    bool preventPriorEffect;

    //Объект пламени
    EFlame *flame;

    // конструктор
    AbstractFlameEffect(
            EFlame *flame, bool preventPriorEffect);


    // метод, выполняющий действие по срабатыванию эффекта
    virtual void apply();


    virtual void applyEffect();

    virtual float getProbabilityCoef();

};


#endif //EFLAME328_FLAMEEFFECT_H
