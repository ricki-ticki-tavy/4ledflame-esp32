//
// Created by dsporykhin on 29.02.20.
//

#include "HotAgeFlameEffect.h"
#include "Defines.h"
#include "Logger.h"
#include "../../../../../.platformio/packages/framework-arduinoespressif32/cores/esp32/Arduino.h"

HotAgeFlameEffect::HotAgeFlameEffect(EFlame *eflame) : AbstractFlameEffect(eflame, true) {

}

void HotAgeFlameEffect::applyEffect() {
    int index = random(0, 4);
    flame->stepCountToDo = random(flame->getSettingsManager()->getSettings()->activeProfile.hotAgeSettings.minStepCount,
                                  flame->getSettingsManager()->getSettings()->activeProfile.hotAgeSettings.maxStepCount + 1);

    double brightDelta = (flame->doubleRandom() *
          (flame->getSettingsManager()->getSettings()->activeProfile.hotAgeSettings.maxBrightnessChangePrct
           - flame->getSettingsManager()->getSettings()->activeProfile.hotAgeSettings.minBrightnessChangePrct)
          + flame->getSettingsManager()->getSettings()->activeProfile.hotAgeSettings.minBrightnessChangePrct)
    * (double)PWM_MAX_WIDTH
    / (double) 100
    + flame->getAbsCurrentNormalBright() - flame->brights[index];

    flame->brightDelta[index] = brightDelta / (double) flame->stepCountToDo;
//    LOGGER.debug("HotAgeFlameEffect  index=" + String(index) + " brgt=" + String(bright) + "   speed=" + String(speed) +
//                 "  totalDelta=" + String(brightDelta));
}

float HotAgeFlameEffect::getProbabilityCoef() {
    return flame->getSettingsManager()->getSettings()->activeProfile.hotAgeSettings.probabilityCoef;
}
