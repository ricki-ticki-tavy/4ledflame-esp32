//
// Created by dsporykhin on 19.04.20.
//

#include "WiFiController.h"
#include "Logger.h"
#include "../../../../../.platformio/packages/framework-arduinoespressif32/libraries/WiFi/src/WiFi.h"
#include "../../../../../.platformio/packages/framework-arduinoespressif32/tools/sdk/include/esp32/esp_wifi_types.h"
#include "../../../../../.platformio/packages/framework-arduinoespressif32/cores/esp32/esp32-hal.h"
#include "../../../../../.platformio/packages/framework-arduinoespressif32/cores/esp32/HardwareSerial.h"
#include "../../../../../.platformio/packages/framework-arduinoespressif32/tools/sdk/include/esp32/esp_event_legacy.h"
#include "../../../../../.platformio/packages/framework-arduinoespressif32/tools/sdk/include/mdns/mdns.h"
#include "../../../../../.platformio/packages/framework-arduinoespressif32/tools/sdk/include/tcpip_adapter/tcpip_adapter.h"

SettingsManager *WiFiController::settingsManager;
bool WiFiController::needReconnect = false;
bool WiFiController::wasSuccedConnected = false;
bool WiFiController::reconnectorActive = false;

WiFiController::WiFiController(SettingsManager *settingsManager) {
    this->settingsManager = settingsManager;
    init();
}

void WiFiController::handle(){
    if (needReconnect && !WiFi.isConnected()){
        tryToConnectToRouter();
    }
}

void WiFiController::setApMode(GlobalSettings *settings, IPAddress *ipAddress, bool defaultApParam = false) {
    WiFi.softAPsetHostname(&settings->network.hostName[0]);
    WiFi.mode(WIFI_AP);
    if (defaultApParam) {
        WiFi.softAP(WIFI_DEFAULT_SID, WIFI_DEFAULT_PASSWORD, 1, 0, 8);
    } else {
        WiFi.softAP(settings->network.ssid, settings->network.password, 1, 0, 8);
    }
    delay(10);

    WiFi.softAPConfig(*ipAddress, *ipAddress, IPAddress(255, 255, 255, 0));
}

void WiFiController::onWiFiEvent(WiFiEvent_t event) {
    switch (event) {
        case SYSTEM_EVENT_STA_START:
            break;
        case SYSTEM_EVENT_STA_GOT_IP:
            break;
        case SYSTEM_EVENT_STA_DISCONNECTED:
            Serial.println("Disconnected from Wi-Fi. reconnecting...");
            needReconnect = reconnectorActive;
            break;
    }
}
//======================================================================================

void WiFiController::tryToConnectToRouter(){

    GlobalSettings *settings = settingsManager->getSettings();

    char testName[] = "                                                                ";
    memset(&testName[0], 0, 64);
    memcpy(&testName[0], &settings->network.hostName[0], strlen(&settings->network.hostName[0]));

    Serial.println("Bring up the WiFi module ");

    // Остановить переподключатель, чтобы не лез
    reconnectorActive = false;

    WiFi.disconnect();
    delay(200);

    WiFi.config(INADDR_NONE, INADDR_NONE, INADDR_NONE);
    WiFi.setHostname(testName);

    WiFi.begin(settings->network.ssid, settings->network.password);

    WiFi.config(INADDR_NONE, INADDR_NONE, INADDR_NONE);
    WiFi.setHostname(testName);

    Serial.println("connecting to router...");

    long startedAt = millis();
    while (!WiFi.isConnected() && (millis() - startedAt < 10000)) {
        delay(20);
    }
    if (!WiFi.isConnected()) {
        // не подключились. В режим AP, если не было однажды успешного подключения
        if (!wasSuccedConnected) {
            Serial.println("Not connected. Switching to AP mode");

            IPAddress ipAddress = IPAddress(settings->network.ipAddress[0],
                                            settings->network.ipAddress[1],
                                            settings->network.ipAddress[2],
                                            settings->network.ipAddress[3]);

            setApMode(settings, &ipAddress, true);
        } else {
            reconnectorActive = true;
            needReconnect = true;
        }
    } else {
        wasSuccedConnected = true;
        Serial.println("Connected to router.");
        Serial.println("local IP " + WiFi.localIP().toString());
        // реконнектор активировать
        reconnectorActive = true;
    }

}
//======================================================================================

void WiFiController::init() {
    Serial.println("init WiFi");
    delay(800);



    GlobalSettings *settings = settingsManager->getSettings();

    IPAddress ipAddress = IPAddress(settings->network.ipAddress[0],
                                    settings->network.ipAddress[1],
                                    settings->network.ipAddress[2],
                                    settings->network.ipAddress[3]);

    WiFi.onEvent(onWiFiEvent);

    LOGGER.info("WiFi  \r\nSID: " + String(&settings->network.ssid[0]) + "\r\n"
            " password: " + String(&settings->network.password[0]) + "\r\n"
                        " hostName: " + String(settings->network.hostName) + "\r\n"
                        " local Ip: " + ipAddress.toString() + "\r\n"
                        " Current state: " + String(WiFi.getMode()) + "\r\n"
                        " wifi mode: " + String(settings->network.wifiMode) + "\r\n");

    if (settings->network.wifiMode != WIFI_OFF) {

        LOGGER.info("Configuring WiFi...");

        if (settings->network.wifiMode == 1) {
            setApMode(settings, &ipAddress);
        } else {
            tryToConnectToRouter();
        }

        Serial.println("bring up MDNS ");

        esp_err_t mDnsError = mdns_init();
        if (mDnsError){
            Serial.println("mDns start error " + String(mDnsError));
        } else {
            mdns_hostname_set(settings->network.hostName);
            mdns_instance_name_set(settings->network.hostName);
            Serial.println("mDns started ");
        }


        Serial.println("Prepare web server ");
        serverController = new WebServerController(settingsManager);

        LOGGER.info("WiFi and services works fine");

    } else if (settings->network.wifiMode == WIFI_OFF && WiFi.getMode() != WIFI_OFF) {

        LOGGER.info("   Turning WiFi off...");
        WiFi.mode(WIFI_OFF);
        delay(1);
//        WiFi.forceSleepBegin();
        delay(1);

    }
}
//======================================================================================
