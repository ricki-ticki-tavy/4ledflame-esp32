//
// Created by dsporykhin on 29.02.20.
//

#include "AvgBrightSweepFlameEffect.h"
#include "Logger.h"

AvgBrightChangeFlameEffect::AvgBrightChangeFlameEffect(EFlame *eflame) : AbstractFlameEffect(eflame,
                                                                                             true) {

}

void AvgBrightChangeFlameEffect::applyEffect() {
    flame->stepCountToDo = 0;
    flame->setCurrentNormalPrightPrct(
            flame->getSettingsManager()->getSettings()->activeProfile.flameSettings.centerBrightnessPrct
            + flame->doubleRandom()
              *
              (flame->getSettingsManager()->getSettings()->activeProfile.avgBrightSweepSettings.maxFadeDownPrct
               +
               flame->getSettingsManager()->getSettings()->activeProfile.avgBrightSweepSettings.maxFadeUpPrct) -
            flame->getSettingsManager()->getSettings()->activeProfile.avgBrightSweepSettings.maxFadeDownPrct);
    LOGGER.debug("AvgBrightChangeFlameEffect  " + String(flame->getCurrentNormalPrightPrct()));
}

float AvgBrightChangeFlameEffect::getProbabilityCoef(){
    return flame->getSettingsManager()->getSettings()->activeProfile.avgBrightSweepSettings.probabilityCoef;
}
