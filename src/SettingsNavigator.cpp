//
// Created by dsporykhin on 24.04.20.
//

#include "SettingsNavigator.h"
#include "Logger.h"
#include "../../../../../.platformio/packages/framework-arduinoespressif32/cores/esp32/IPAddress.h"
#include "../../../../../.platformio/packages/framework-arduinoespressif32/cores/esp32/HardwareSerial.h"

SettingsNavigator::SettingsNavigator(SettingsManager *settingsManager) {
    this->settingsManager = settingsManager;
    this->settings = settingsManager->getSettings();

    this->paramDescriptors[activeParamDescriptors++] = new ParamDescriptor("globalBrightnessPrct", FLOAT, 0, 100,
                                                                           (void *) &settings->globalBrightnessPrct,
                                                                           (void *) &settings->globalBrightnessPrct);

    this->paramDescriptors[activeParamDescriptors++] = new ParamDescriptor("flame>loopIntervalMs", INTEGER, 1, 300,
                                                                           (void *) &settings->activeProfile.flameSettings.loopIntervalMs,
                                                                           (void *) &tempProfile.flameSettings.loopIntervalMs);
    this->paramDescriptors[activeParamDescriptors++] = new ParamDescriptor("flame>shutdownAfterSec", INTEGER, 0, 78600,
                                                                           (void *) &settings->activeProfile.flameSettings.shutdownAfterSec,
                                                                           (void *) &tempProfile.flameSettings.shutdownAfterSec);
    this->paramDescriptors[activeParamDescriptors++] = new ParamDescriptor("flame>brightnessCoefficientPrct", FLOAT, 0,
                                                                           100,
                                                                           (void *) &settings->activeProfile.flameSettings.brightnessCoefficientPrct,
                                                                           (void *) &tempProfile.flameSettings.brightnessCoefficientPrct);
    this->paramDescriptors[activeParamDescriptors++] = new ParamDescriptor("flame>centerBrightnessPrct", FLOAT, 0, 100,
                                                                           (void *) &settings->activeProfile.flameSettings.centerBrightnessPrct,
                                                                           (void *) &tempProfile.flameSettings.centerBrightnessPrct);
    this->paramDescriptors[activeParamDescriptors++] = new ParamDescriptor("flame>newEffectProbabilityPer100CyclesPrct",
                                                                           FLOAT, 0, 100,
                                                                           (void *) &settings->activeProfile.flameSettings.newEffectProbabilityPer100CyclesPrct,
                                                                           (void *) &tempProfile.flameSettings.newEffectProbabilityPer100CyclesPrct);
    this->paramDescriptors[activeParamDescriptors++] = new ParamDescriptor("flame>probabilityChangeActionPrct", FLOAT,
                                                                           0, 100,
                                                                           (void *) &settings->activeProfile.flameSettings.probabilityChangeActionPrct,
                                                                           (void *) &tempProfile.flameSettings.probabilityChangeActionPrct);


    this->paramDescriptors[activeParamDescriptors++] = new ParamDescriptor("normalize>minStepCount", INTEGER, 2, 300,
                                                                           (void *) &settings->activeProfile.normalizeSettings.minStepCount,
                                                                           (void *) &tempProfile.normalizeSettings.minStepCount);
    this->paramDescriptors[activeParamDescriptors++] = new ParamDescriptor("normalize>maxStepCount", INTEGER, 2, 300,
                                                                           (void *) &settings->activeProfile.normalizeSettings.maxStepCount,
                                                                           (void *) &tempProfile.normalizeSettings.maxStepCount);
    this->paramDescriptors[activeParamDescriptors++] = new ParamDescriptor("normalize>probabilityCoef", FLOAT, 0, 10,
                                                                           (void *) &settings->activeProfile.normalizeSettings.probabilityCoef,
                                                                           (void *) &tempProfile.normalizeSettings.probabilityCoef);


    this->paramDescriptors[activeParamDescriptors++] = new ParamDescriptor("hotAge>probabilityCoef", FLOAT, 0, 10,
                                                                           (void *) &settings->activeProfile.hotAgeSettings.probabilityCoef,
                                                                           (void *) &tempProfile.hotAgeSettings.probabilityCoef);
    this->paramDescriptors[activeParamDescriptors++] = new ParamDescriptor("hotAge>minBrightnessChangePrct", FLOAT, 0,
                                                                           70,
                                                                           (void *) &settings->activeProfile.hotAgeSettings.minBrightnessChangePrct,
                                                                           (void *) &tempProfile.hotAgeSettings.minBrightnessChangePrct);
    this->paramDescriptors[activeParamDescriptors++] = new ParamDescriptor("hotAge>maxBrightnessChangePrct", FLOAT, 0,
                                                                           70,
                                                                           (void *) &settings->activeProfile.hotAgeSettings.maxBrightnessChangePrct,
                                                                           (void *) &tempProfile.hotAgeSettings.maxBrightnessChangePrct);
    this->paramDescriptors[activeParamDescriptors++] = new ParamDescriptor("hotAge>minStepCount", INTEGER, 1, 50,
                                                                           (void *) &settings->activeProfile.hotAgeSettings.minStepCount,
                                                                           (void *) &tempProfile.hotAgeSettings.minStepCount);
    this->paramDescriptors[activeParamDescriptors++] = new ParamDescriptor("hotAge>maxStepCount", INTEGER, 1, 50,
                                                                           (void *) &settings->activeProfile.hotAgeSettings.maxStepCount,
                                                                           (void *) &tempProfile.hotAgeSettings.maxStepCount);


    this->paramDescriptors[activeParamDescriptors++] = new ParamDescriptor("avgBrightSweep>maxFadeDownPrct", FLOAT, 0,
                                                                           50,
                                                                           (void *) &settings->activeProfile.avgBrightSweepSettings.maxFadeDownPrct,
                                                                           (void *) &tempProfile.avgBrightSweepSettings.maxFadeDownPrct);
    this->paramDescriptors[activeParamDescriptors++] = new ParamDescriptor("avgBrightSweep>maxFadeUpPrct", FLOAT, 0,
                                                                           50,
                                                                           (void *) &settings->activeProfile.avgBrightSweepSettings.maxFadeUpPrct,
                                                                           (void *) &tempProfile.avgBrightSweepSettings.maxFadeUpPrct);
    this->paramDescriptors[activeParamDescriptors++] = new ParamDescriptor("avgBrightSweep>probabilityCoef", FLOAT, 0,
                                                                           10,
                                                                           (void *) &settings->activeProfile.avgBrightSweepSettings.probabilityCoef,
                                                                           (void *) &tempProfile.avgBrightSweepSettings.probabilityCoef);

    this->paramDescriptors[activeParamDescriptors++] = new ParamDescriptor("tremble>minBrightnessChangePrct", FLOAT, 0,
                                                                           40,
                                                                           (void *) &settings->activeProfile.trembleSettings.minBrightnessChangePrct,
                                                                           (void *) &tempProfile.trembleSettings.minBrightnessChangePrct);
    this->paramDescriptors[activeParamDescriptors++] = new ParamDescriptor("tremble>maxBrightnessChangePrct", FLOAT, 0,
                                                                           40,
                                                                           (void *) &settings->activeProfile.trembleSettings.maxBrightnessChangePrct,
                                                                           (void *) &tempProfile.trembleSettings.maxBrightnessChangePrct);
    this->paramDescriptors[activeParamDescriptors++] = new ParamDescriptor("tremble>minStepCount", INTEGER, 2, 300,
                                                                           (void *) &settings->activeProfile.trembleSettings.minStepCount,
                                                                           (void *) &tempProfile.trembleSettings.minStepCount);
    this->paramDescriptors[activeParamDescriptors++] = new ParamDescriptor("tremble>maxStepCount", INTEGER, 2, 300,
                                                                           (void *) &settings->activeProfile.trembleSettings.maxStepCount,
                                                                           (void *) &tempProfile.trembleSettings.maxStepCount);
    this->paramDescriptors[activeParamDescriptors++] = new ParamDescriptor("tremble>matrix", FLOAT, -10, 10, 32,
                                                                           (void *) &settings->activeProfile.trembleSettings.matrix,
                                                                           (void *) &tempProfile.trembleSettings.matrix);
    this->paramDescriptors[activeParamDescriptors++] = new ParamDescriptor("tremble>probabilityCoef", FLOAT, 0,
                                                                           10,
                                                                           (void *) &settings->activeProfile.trembleSettings.probabilityCoef,
                                                                           (void *) &tempProfile.trembleSettings.probabilityCoef);

    this->paramDescriptors[activeParamDescriptors++] = new ParamDescriptor("network>wifiMode", UCHAR, 0,
                                                                           10,
                                                                           (void *) &settings->network.wifiMode,
                                                                           (void *) &settings->network.wifiMode);
    this->paramDescriptors[activeParamDescriptors++] = new ParamDescriptor("network>ssid", STRING, 5,
                                                                           63,
                                                                           (void *) &settings->network.ssid[0],
                                                                           (void *) &settings->network.ssid[0]);
    this->paramDescriptors[activeParamDescriptors++] = new ParamDescriptor("network>password", STRING, 5,
                                                                           63,
                                                                           (void *) &settings->network.password[0],
                                                                           (void *) &settings->network.password[0]);
    this->paramDescriptors[activeParamDescriptors++] = new ParamDescriptor("network>hostName", STRING, 5,
                                                                           63,
                                                                           (void *) &settings->network.hostName[0],
                                                                           (void *) &settings->network.hostName[0]);
    this->paramDescriptors[activeParamDescriptors++] = new ParamDescriptor("network>ipAddress", IPv4, 7,
                                                                           15,
                                                                           (void *) &settings->network.ipAddress[0],
                                                                           (void *) &settings->network.ipAddress[0]);

    this->paramDescriptors[activeParamDescriptors++] = new ParamDescriptor("activeProfile>name", STRING, 7,
                                                                           15,
                                                                           (void *) &settings->activeProfile.name[0],
                                                                           (void *) &settings->activeProfile.name[0]);


}
//--------------------------------------------------------------------

String SettingsNavigator::getSettingByName(String origParamName) {
    String paramName = origParamName;

    if (origParamName == "profilesList"){
        return getUserProfilesList();
    }

    bool showMinR = paramName.endsWith("#minR");
    bool showMin = paramName.endsWith("#min");
    bool showMaxR = paramName.endsWith("#maxR");
    bool showMax = paramName.endsWith("#max");

    // проверим на наличие #min или #max в конце имени переменной
    if (showMax || showMin) {
        paramName.remove(origParamName.length() - 4);
    }  else if (showMaxR || showMinR) {
        paramName.remove(origParamName.length() - 5);
    }

    for (int descriptorIndex = 0; descriptorIndex < activeParamDescriptors; descriptorIndex++) {
        if (paramDescriptors[descriptorIndex]->paramName == paramName) {
            // Нашли наш параметер
            if (paramDescriptors[descriptorIndex]->arraySize == 0 || showMax || showMin || showMaxR || showMinR) {
                if (paramDescriptors[descriptorIndex]->paramType == INTEGER || showMaxR || showMinR) {
                    return showMin || showMax || showMinR || showMaxR
                           ? (showMin || showMinR ? String((int) paramDescriptors[descriptorIndex]->minValue) : String(
                                    (int) paramDescriptors[descriptorIndex]->maxValue))
                           : String(*(int *) paramDescriptors[descriptorIndex]->valueReferenceForRead);
                } else if (paramDescriptors[descriptorIndex]->paramType == FLOAT) {
                    return showMin || showMax
                           ? (showMin ? String(paramDescriptors[descriptorIndex]->minValue) : String(
                                    paramDescriptors[descriptorIndex]->maxValue))
                           : String(*(float *) paramDescriptors[descriptorIndex]->valueReferenceForRead);
                } else if (paramDescriptors[descriptorIndex]->paramType == STRING){
                    return showMin || showMax
                           ? (showMin ? String((int)paramDescriptors[descriptorIndex]->minValue) : String(
                                    (int)paramDescriptors[descriptorIndex]->maxValue))
                           : String((char *)paramDescriptors[descriptorIndex]->valueReferenceForRead);
                } else if (paramDescriptors[descriptorIndex]->paramType == UCHAR){
                    return showMin || showMax
                           ? (showMin ? String((unsigned char)paramDescriptors[descriptorIndex]->minValue) : String(
                                    (unsigned char)paramDescriptors[descriptorIndex]->maxValue))
                           : String(*(unsigned char *)paramDescriptors[descriptorIndex]->valueReferenceForRead);
                } else if (paramDescriptors[descriptorIndex]->paramType == IPv4){
                    String ipAsString;

                    if (!showMin && !showMax){
                        unsigned char *ipRaw = (unsigned char *)paramDescriptors[descriptorIndex]->valueReferenceForRead;
                        IPAddress ipV4 = IPAddress(ipRaw[0], ipRaw[1], ipRaw[2], ipRaw[3]);
                        ipAsString = ipV4.toString();
                    }
                    return showMin || showMax
                           ? (showMin ? String((unsigned char)paramDescriptors[descriptorIndex]->minValue) : String(
                                    (unsigned char)paramDescriptors[descriptorIndex]->maxValue))
                           : ipAsString;
                }
            } else {
                //  array
                String paramNameTitle = paramName;
                paramNameTitle.replace('>', '_');
                String result = "{\"" + paramNameTitle + "\":[";

                if (paramDescriptors[descriptorIndex]->paramType == INTEGER) {
                    int *arrayRef = (int *) paramDescriptors[descriptorIndex]->valueReferenceForRead;

                    for (int i = 0; i < paramDescriptors[descriptorIndex]->arraySize; i++) {
                        if (i != 0) {
                            result.concat(",");
                        }
                        result.concat(String(arrayRef[i]));
                    }
                } else if (paramDescriptors[descriptorIndex]->paramType == FLOAT) {
                    float *arrayRef = (float *) paramDescriptors[descriptorIndex]->valueReferenceForRead;

                    for (int i = 0; i < paramDescriptors[descriptorIndex]->arraySize; i++) {
                        if (i != 0) {
                            result.concat(",");
                        }
                        result.concat(String(arrayRef[i]));
                    }
                }
                result.concat("]}");
                return result;
            }
        }
    }

    LOGGER.error("parameter " + paramName + " not found");
    return "bad parameter name";

}
//--------------------------------------------------------------------

bool SettingsNavigator::activateProfile(String profileName) {
    for (int i = 0; i < MAX_PROFILE_COUNTER; i++) {
        if (!settings->userProfiles[i].isFree
            && String(settings->userProfiles[i].name) == profileName) {
            memcpy(&settings->activeProfile, &settings->userProfiles[i], sizeof(Profile));

            return true;
        }
    }

    return false;
}
//--------------------------------------------------------------------

String SettingsNavigator::saveProfile(String profileName) {
    Serial.println("saveProfile");
    int foundIndex = -1;
    for (int searchIndex = SYSTEM_PROFILE_COUNTER; searchIndex < MAX_PROFILE_COUNTER; searchIndex++) {
        if (!settings->userProfiles[searchIndex].isFree
            && String(settings->userProfiles[searchIndex].name) == profileName) {

            if (settings->userProfiles[searchIndex].system) { // нельзя перезаписать системный профиль
                Serial.println("profile \"" + profileName + "\" is system");
                return "Профиль \"" + profileName + "\" встроенный. Редактирование невозможно";
            }
            Serial.println("profile \"" + profileName + "\" was found");
            foundIndex = searchIndex;
            break;
        }
    }

    if (foundIndex < 0){
        Serial.println("profile \"" + profileName + "\" was NOT found. Looking for free slot");
        // ищем свободный слот
        for (int searchIndex = SYSTEM_PROFILE_COUNTER; searchIndex < MAX_PROFILE_COUNTER; searchIndex++){
            if (settings->userProfiles[searchIndex].isFree) {
                Serial.println("profile \"" + profileName + "\" free slot found.");
                foundIndex = searchIndex;
                break;
            }
        }
    };

    if (foundIndex > 0){
        // скопируем текущий профиль в найденный слот
        Serial.println("copying profile from active. (" + String((int)sizeof(Profile)) + ")");
        memcpy(&settings->userProfiles[foundIndex], &settings->activeProfile, sizeof(Profile));
        Serial.println("Preparing fields ");
        profileName.toCharArray(&settings->userProfiles[foundIndex].name[0], sizeof(settings->userProfiles[foundIndex].name));
        settings->userProfiles[foundIndex].isFree = false;
        settings->userProfiles[foundIndex].system = false;

        Serial.println("naming active profile");
        // имя текущего профиля выставить
        profileName.toCharArray(&settings->activeProfile.name[0], sizeof(settings->userProfiles[foundIndex].name));

        Serial.println("prepare to read EEPROM");
        // Сохраним через перезачитывание из EEPROM
        // считаем текущие настройки как они в EEPROM. Это надо, чтобы при записи не закинуть с новым профилем
        // и текущее состояние прочих настроек и изменения из текущего активного профиля
        Serial.println("reading EEPROM");
        settingsManager->readSettings(&tempSettings);
        Serial.println("copiyng profile to EEPROM buffer");
        memcpy(&tempSettings.userProfiles[foundIndex], &settings->userProfiles[foundIndex], sizeof(Profile));

        Serial.println("saving EEPROM");
        // Обратно запишем с одним измененным профилем
        settingsManager->saveSetting(&tempSettings, false);
        Serial.println("saving DONE");

        return "";

    } else {
        return "Нет свободных слотов (макс. " + String(MAX_PROFILE_COUNTER) + ")";
    }
}
//--------------------------------------------------------------------

String SettingsNavigator::removeProfile() {
    if (String(&settings->activeProfile.name[0]).startsWith("*")) {
        return "Профильбыл изменен. Возможно удаление оригинального профиля.";
    } else if (settings->activeProfile.system) {
        return "Невозможно удаление встроенного профиля.";
    } else {
        String profileName = String(&settings->activeProfile.name[0]);
        for (int searchIndex = SYSTEM_PROFILE_COUNTER; searchIndex < MAX_PROFILE_COUNTER; searchIndex++){
            if (String(settings->userProfiles[searchIndex].name) == profileName){
                // ставим признае свободного слота
                settings->userProfiles[searchIndex].isFree = true;
                // копируем в активный профиль первый системный
                memcpy(&settings->activeProfile, &settings->userProfiles[0], sizeof(Profile));

                // Сохраним изменения
                Serial.println("reading EEPROM");
                settingsManager->readSettings(&tempSettings);
                Serial.println("copiyng profile to EEPROM buffer");
                memcpy(&tempSettings.activeProfile, &settings->activeProfile, sizeof(Profile));
                tempSettings.userProfiles[searchIndex].isFree = true;

                Serial.println("saving EEPROM");
                // Обратно запишем с одним измененным профилем
                settingsManager->saveSetting(&tempSettings, false);
                Serial.println("saving DONE");
            }
        }
    }
}
//--------------------------------------------------------------------

void SettingsNavigator::saveNetworkSettingsAndRestart(NetworkSettings *networkSettings) {
    LOGGER.warning("Saving new network settings:\r\n"
                           "        SSID: " + String(networkSettings->ssid) + "\r\n"
                           "    password: " + String(networkSettings->password) + "\r\n"
                           "   host name: " + String(networkSettings->hostName) + "\r\n"
                           "    local Ip: " + String(networkSettings->ipAddress[0]) + "." +
                   String(networkSettings->ipAddress[1]) + "." +
                   String(networkSettings->ipAddress[2]) + "." +
                   String(networkSettings->ipAddress[3]) + "\r\n"
                           "   wifi mode: " + String(networkSettings->wifiMode));

    memcpy(&settings->network, networkSettings, sizeof(NetworkSettings));

    settingsManager->saveSetting(true);
}
//--------------------------------------------------------------------


String SettingsNavigator::saveSettingsByNames(String *params, int paramsCount) {
    String res = "";

    memcpy(&tempProfile, &settings->activeProfile, sizeof(Profile));

    for (int paramIndex = 0; paramIndex < paramsCount; paramIndex++) {
        String param = params[paramIndex];
        param.trim();

        if (!param.isEmpty()) {
// отделим значение от имени параметра
            String invalidParameterMessage = ("Invalid parameter format (\"" + param + "\"");
            int separatorIndex = param.indexOf('=');
            if (separatorIndex == -1) {
                return invalidParameterMessage;
            }

            String paramName = param.substring(0, separatorIndex);
            String value = param.substring(separatorIndex + 1);

            paramName.trim();
            value.trim();

            if (param.isEmpty() || value.isEmpty()) {
                return invalidParameterMessage;
            }

            res = saveSettingByName(paramName, value);
        }
        // Если ошибка записи переменной, то далее не продолжаем
        if (res != "") {
            return res;
        }
    }

    // Если все норм, то комитим изменения
    if (res == "") {
        memcpy(&settings->activeProfile, &tempProfile, sizeof(Profile));

        // сброс имени профиля. Теперь он пользовательский
        if (!String(settingsManager->getSettings()->activeProfile.name).startsWith("*")) {
            String("*" + String(settingsManager->getSettings()->activeProfile.name)).toCharArray(&settingsManager->getSettings()->activeProfile.name[0], 64);
        }

    }

    return res;
}
//--------------------------------------------------------------------

String SettingsNavigator::saveSettingByName(String paramName, String value) {
    for (int descriptorIndex = 0; descriptorIndex < activeParamDescriptors; descriptorIndex++) {
        if (paramDescriptors[descriptorIndex]->paramName == paramName) {
            // Нашли наш параметер
            if (paramDescriptors[descriptorIndex]->arraySize == 0) {
                if (paramDescriptors[descriptorIndex]->paramType == INTEGER) {
                    int intValue = value.toInt();
                    if ((intValue < paramDescriptors[descriptorIndex]->minValue)
                        || (intValue > paramDescriptors[descriptorIndex]->maxValue)) {
                        return "Value of \"" + paramName + "\" is not in diapason from "
                               + String(paramDescriptors[descriptorIndex]->minValue) + " to "
                               + String(paramDescriptors[descriptorIndex]->maxValue);
                    } else {
                        *((int *) paramDescriptors[descriptorIndex]->valueReferenceForWrite) = intValue;
                    }
                } else if (paramDescriptors[descriptorIndex]->paramType == FLOAT) {
                    float floatValue = value.toFloat();
                    if ((floatValue < paramDescriptors[descriptorIndex]->minValue)
                        || (floatValue > paramDescriptors[descriptorIndex]->maxValue)) {
                        return "Value of \"" + paramName + "\" is not in diapason from "
                               + String(paramDescriptors[descriptorIndex]->minValue) + " to "
                               + String(paramDescriptors[descriptorIndex]->maxValue);
                    } else {
                        *((float *) paramDescriptors[descriptorIndex]->valueReferenceForWrite) = floatValue;
                    }
                }
            } else {
                // массив
                if (paramDescriptors[descriptorIndex]->paramType == FLOAT) {

                    char elementSeparator = ',';

                    int indexOfParamSeparator = value.indexOf(elementSeparator);
                    int startCopyIndex = 0;
                    int matrixIndex = 0;
                    String strValue;
                    if (value.indexOf(elementSeparator) >= 0)
                        do {
                            strValue = indexOfParamSeparator == -1
                                       ? value.substring(startCopyIndex)
                                       : value.substring(startCopyIndex, indexOfParamSeparator);
                            startCopyIndex = indexOfParamSeparator + 1;

                            float *refMatrix = (float *) paramDescriptors[descriptorIndex]->valueReferenceForWrite;
                            float floatValue = strValue.toFloat();

                            if ((floatValue < paramDescriptors[descriptorIndex]->minValue)
                                || (floatValue > paramDescriptors[descriptorIndex]->maxValue)) {
                                return "Value of \"" + paramName + "\" is not in diapason from "
                                       + String(paramDescriptors[descriptorIndex]->minValue) + " to "
                                       + String(paramDescriptors[descriptorIndex]->maxValue);
                            }
                            refMatrix[matrixIndex] = floatValue;
                            matrixIndex++;
                        } while ((indexOfParamSeparator = value.indexOf(elementSeparator, indexOfParamSeparator + 1)) >=
                                 0 &&
                                 matrixIndex < 32);
                    // последний элемент
                    if (startCopyIndex < value.length() && matrixIndex < 32) {
                        strValue = indexOfParamSeparator == -1
                                   ? value.substring(startCopyIndex)
                                   : value.substring(startCopyIndex, indexOfParamSeparator);

                        float *refMatrix = (float *) paramDescriptors[descriptorIndex]->valueReferenceForWrite;
                        float floatValue = strValue.toFloat();

                        if ((floatValue < paramDescriptors[descriptorIndex]->minValue)
                            || (floatValue > paramDescriptors[descriptorIndex]->maxValue)) {
                            return "Value of \"" + paramName + "\" is not in diapason from "
                                   + String(paramDescriptors[descriptorIndex]->minValue) + " to "
                                   + String(paramDescriptors[descriptorIndex]->maxValue);
                        }
                        refMatrix[matrixIndex] = floatValue;
                    }
                }
            }

            return "";
        }
    }
    return "parameter \"" + paramName + "\" not found";
}
//--------------------------------------------------------------------

String SettingsNavigator::getUserProfilesList(){
    String res = "{\"activeProfileName\":\"" + String(settings->activeProfile.name) + "\", \"profiles\":[";
    for (int profileIndex = 0; profileIndex < MAX_PROFILE_COUNTER; profileIndex++){
        if (!settings->userProfiles[profileIndex].isFree) {
            if (profileIndex != 0)
                res.concat(",");
            res.concat("{\"name\":\"" + String(settings->userProfiles[profileIndex].name) + "\",\"id\":" + String(profileIndex) + ",\"sys\":\"" + String(settings->userProfiles[profileIndex].system) + "\"}");
        }
    }
    res.concat("]}");
    return res;
}
//--------------------------------------------------------------------
