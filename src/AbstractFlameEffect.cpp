//
// Created by dsporykhin on 29.02.20.
//

#include "AbstractFlameEffect.h"

class EFlame;

AbstractFlameEffect::AbstractFlameEffect(EFlame *flame
        , bool preventPriorEffect) {
    this->preventPriorEffect = preventPriorEffect;
    this->flame = flame;
}

void AbstractFlameEffect::apply() {
    if (preventPriorEffect) {
        for (int index = 0; index < 4; index++) {
            flame->brightDelta[index] = 0;
        }
    }
    applyEffect();
}

void AbstractFlameEffect::applyEffect(){

}

float AbstractFlameEffect::getProbabilityCoef(){
    return 0;
};