//
// Created by dsporykhin on 08.05.20.
//

#include "WebUpdater.h"
#include "../../../../../.platformio/packages/framework-arduinoespressif32/libraries/Update/src/Update.h"
#include "../../../../../.platformio/packages/framework-arduinoespressif32/cores/esp32/Esp.h"

void WebUpdater::updateApiProcessor(AsyncWebServerRequest *request) {
    AsyncAbstractResponse *responce = new AsyncAbstractResponse();
    responce->addHeader("Connection", "close");
    responce->setCode(Update.hasError() ? 500 : 200);
    request->send(responce);
    ESP.restart();
}

void WebUpdater::updateFuncApiProcessor(AsyncWebServerRequest *request, const String& filename, size_t index, uint8_t *data, size_t len, bool final) {
//        HTTPUpload &upload = server->upload();
//        if (upload.status == UPLOAD_FILE_START) {
//            Serial.printf("Update: %s\n", upload.filename.c_str());
//            if (!Update.begin(UPDATE_SIZE_UNKNOWN)) { //start with max available size
//                Update.printError(Serial);
//            }
//        } else if (upload.status == UPLOAD_FILE_WRITE) {
//            /* flashing firmware to ESP*/
//            if (Update.write(upload.buf, upload.currentSize) != upload.currentSize) {
//                Update.printError(Serial);
//            }
//        } else if (upload.status == UPLOAD_FILE_END) {
//            if (Update.end(true)) { //true to set the size to the current progress
//                Serial.printf("Update Success: %u\nRebooting...\n", upload.totalSize);
//            } else {
//                Update.printError(Serial);
//            }
//        }
//    });
//
//    server->begin(8080);
}

