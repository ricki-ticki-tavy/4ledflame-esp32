//
// Created by dsporykhin on 24.04.20.
//

#ifndef EFLAMEESP8266_SETTINGSNAVIGATOR_H
#define EFLAMEESP8266_SETTINGSNAVIGATOR_H


#include "SettingsManager.h"
#include "ParamDescriptor.h"
#include "../../../../../.platformio/packages/framework-arduinoespressif32/cores/esp32/WString.h"

class ParamDescriptor;

class SettingsManager;

/**
 * Класс для чтения и сохранения настроек по их именам
 */
class SettingsNavigator {
private:
    SettingsManager *settingsManager;
    GlobalSettings *settings;
    Profile tempProfile;
    ParamDescriptor *paramDescriptors[50];
    int activeParamDescriptors = 0;

    String saveSettingByName(String paramName, String value);


public:
    SettingsNavigator(SettingsManager *settingsManager);


    GlobalSettings tempSettings;

    /**
     * Сохранить новые сетевые настройки и перезагрузиться с ними
     * @param networkSettings
     */
    void saveNetworkSettingsAndRestart(NetworkSettings *networkSettings);

    /**
     * Возвращает значение параметра по имени
     * @param paramName
     * @return
     */
    String getSettingByName(String paramName);

    /**
     * Сохранить значения параметров.
     * @param paramNames
     * @param values
     * @return
     */
    String saveSettingsByNames(String *params, int paramsCount);

    /**
     * Активировать профиль
     * @param profileName
     * @return
     */
    bool activateProfile(String profileName);

    /**
     * Сохранить текущие настройки в профиль. Если профиль с таким именем уже есть, то запись будет поверх, если нету,
     * то будет использован первый найденный свободный слот
     * @param profileName
     * @return
     */
    String saveProfile(String profileName);

    /**
     * Удалить текущий профиль.
     * то будет использован первый найденный свободный слот
     * @param profileName
     * @return
     */
    String removeProfile();

    /**
     * Возвращает в JSON список профилей
     * @return
     */
    String getUserProfilesList();

};


#endif //EFLAMEESP8266_SETTINGSNAVIGATOR_H
