//
// Created by dsporykhin on 08.05.20.
//

#ifndef EFLAMEESP32_WEBUPDATER_H
#define EFLAMEESP32_WEBUPDATER_H


#include "../../../../../.platformio/packages/framework-arduinoespressif32/cores/esp32/WString.h"
#include "../lib/ESPAsyncWebServer/ESPAsyncWebServer.h"

class WebUpdater {
public:
    static void updateApiProcessor(AsyncWebServerRequest *request);
    static void updateFuncApiProcessor(AsyncWebServerRequest *request, const String& filename, size_t index, uint8_t *data, size_t len, bool final);
};


#endif //EFLAMEESP32_WEBUPDATER_H
