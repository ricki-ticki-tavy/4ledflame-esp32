//
// Created by dsporykhin on 19.04.20.
//

#include "../../../../../.platformio/packages/framework-arduinoespressif32/cores/esp32/WString.h"
#include "WebServerController.h"
#include "Defines.h"
#include "Logger.h"
#include "../../../../../.platformio/packages/framework-arduinoespressif32/libraries/SPIFFS/src/SPIFFS.h"
#include "../../../../../.platformio/packages/framework-arduinoespressif32/cores/esp32/IPAddress.h"
#include "../../../../../.platformio/packages/framework-arduinoespressif32/libraries/FS/src/FS.h"
#include "../../../../../.platformio/packages/framework-arduinoespressif32/cores/esp32/HardwareSerial.h"
#include "WebUpdater.h"

String TEXT_PLAN = "text/plan";
String TEXT_JSON = "text/json";
String OK_RESPONSE = "OK";
String PROFILES_PARAMETER_ATTR_NAME = "parameter";

SettingsManager *WebServerController::settingsManager;

bool WebServerController::checkFile(String fileName){
    File file = SPIFFS.open(fileName);
    if (file){
        file.close();
        Serial.println("\"" + fileName + "\" exists");
        return true;
    } else {
        Serial.println("file \"" + fileName + "\" does not exists");
        return false;
    }
}

WebServerController::WebServerController(SettingsManager *settingsManager) {

    checkFile("/index.html");

    WebServerController::settingsManager = settingsManager;

    LOGGER.info(" Starting web server...");

    webServer = new AsyncWebServer(80);

    webServer->on("/", HTTP_GET, [](AsyncWebServerRequest *request) {
        Serial.println("root request");
        request->send(SPIFFS, "/index.html", String(), false, systemSettingsProcessor);
    });

    webServer->on("/wifiSettings.html", HTTP_GET, [](AsyncWebServerRequest *request) {
        request->send(SPIFFS, "/wifiSettings.html", String(), false, systemSettingsProcessor);
    });

    webServer->on("/profile.html", HTTP_GET, [](AsyncWebServerRequest *request) {
        request->send(SPIFFS, "/profile.html", String(), false, systemSettingsProcessor);
    });

    webServer->on("/log", HTTP_GET, [](AsyncWebServerRequest *request) {
        request->send(200, "text/html", &LOGGER.logData[0]);
    });

    webServer->on("/update", HTTP_GET, [](AsyncWebServerRequest *request) {
        request->send(SPIFFS, "/update.html", String(), false, nullptr);
    });

    webServer->on("/update", HTTP_POST, WebUpdater::updateApiProcessor, WebUpdater::updateFuncApiProcessor);

    webServer->on("/profilesApi", HTTP_GET, profilesApiProcessor);
    webServer->on("/profilesApi", HTTP_POST, profilesApiProcessor);

    webServer->on("/settingsApi", HTTP_GET, settingsApiProcessor);
    webServer->on("/settingsApi", HTTP_POST, settingsApiProcessor);

    webServer->on("/pinDetectApi", HTTP_GET, pinDetectProcessor);

    webServer->onNotFound(loadFileByUrl);

    webServer->begin();
}


/**
 * Change system settings handler
 * @param request
 */
void WebServerController::pinDetectProcessor(AsyncWebServerRequest *request) {
    String operation = request->arg(PROFILES_OPERATION_ATTR_NAME);
    if (operation.isEmpty()){
        request->send(503, TEXT_PLAN, "\"operation\" can't be empty");
    } else {
        if (operation == "stop"){
            settingsManager->firePaused = true;
            settingsManager->pinValues[0] = 0;
            settingsManager->pinValues[1] = 0;
            settingsManager->pinValues[2] = 0;
            settingsManager->pinValues[3] = 0;
            request->send(200, TEXT_PLAN, OK_RESPONSE);
        } else if (operation == "start"){
            settingsManager->firePaused = false;
            request->send(200, TEXT_PLAN, OK_RESPONSE);
        } else if (operation == "on" || operation == "off"){
            String parameter = request->arg(PROFILES_PARAMETER_ATTR_NAME);
            if (parameter.isEmpty()){
                request->send(503, TEXT_PLAN, "\"" + PROFILES_PARAMETER_ATTR_NAME + "\" can't be empty");
            } else {
                int pinIndex = parameter.toInt();
                if (pinIndex > 4 || pinIndex < 0){
                    request->send(503, TEXT_PLAN, "\"" + PROFILES_PARAMETER_ATTR_NAME + "\" must be between 0 and 3");
                } else {
                    settingsManager->pinValues[pinIndex] = operation == "on" ? PWM_MAX_WIDTH
                            : 0;
                    request->send(200, TEXT_PLAN, OK_RESPONSE);
                }
            }
        }
    }
}

/**
 * Change system settings handler
 * @param request
 */
void WebServerController::settingsApiProcessor(AsyncWebServerRequest *request) {
    String parameterAttrName = "parameter";
    String valueAttrName = "value";

    String operation = request->arg(PROFILES_OPERATION_ATTR_NAME);
    String parameter = request->arg(parameterAttrName);

    LOGGER.debug("apiSettings");

    if (!parameter || parameter.isEmpty()) {
        String message = "\"" + parameterAttrName + "\" can't be null";
        request->send(200, TEXT_PLAN, message);
    } else if (parameter == "saveDefaultProfile") {
        // Сохранение профиля по умолчанию в EEPROM
        LOGGER.info("Saving default profile");
        settingsManager->saveSetting(false);
        request->send(200, TEXT_PLAN, OK_RESPONSE);


    } else if (!operation || operation.isEmpty()) {
        String message = "\"operation\" can't be null";
        request->send(200, TEXT_PLAN, message);
    } else if ((operation != PARAMETER_OPERATION_WRITE) && (operation != PARAMETER_OPERATION_READ)) {
        String message = "invalid \"operation\" value";
        request->send(503, TEXT_PLAN, message);
    } else if (parameter == "wifi") {
        if (operation = PARAMETER_OPERATION_WRITE) {
            String wifiSSID = request->arg("network>ssid");
            String wifiPassword = request->arg("network>password");
            String wifiHostName = request->arg("network>hostName");
            String localIP = request->arg("network>ipAddress");
            String wiFiMode = request->arg("network>wifiMode");

            if (!wifiSSID || wifiSSID.isEmpty()) {
                request->send(503, TEXT_PLAN, "wiFiSsId can't be null");
            } else if (!wifiPassword || wifiPassword.isEmpty()) {
                request->send(503, TEXT_PLAN, "wiFiPassword can't be null");
            } else if (!wifiHostName || wifiHostName.isEmpty()) {
                request->send(503, TEXT_PLAN, "wifiHostName can't be null");
            } else if (!wiFiMode || wiFiMode.isEmpty() || wiFiMode.toInt() > 2 || wiFiMode.toInt() < 0) {
                request->send(503, TEXT_PLAN, "invalid wifiMode");
            } else if (!localIP || localIP.isEmpty()) {
                request->send(503, TEXT_PLAN, "localIP can't be null");
            } else if (!IPAddress(0, 0, 0, 0).fromString(localIP)) {
                request->send(503, TEXT_PLAN, "invalid localIP");
            } else {
                IPAddress localIPAddress = IPAddress(0, 0, 0, 0);
                localIPAddress.fromString(localIP);

                NetworkSettings networkSettings;
                wifiSSID.toCharArray(&networkSettings.ssid[0], sizeof(networkSettings.ssid));
                wifiPassword.toCharArray(&networkSettings.password[0],
                                         sizeof(networkSettings.password));
                wifiHostName.toCharArray(&networkSettings.hostName[0],
                                         sizeof(networkSettings.hostName));
                for (int i = 0; i < 4; i++) {
                    networkSettings.ipAddress[i] = localIPAddress[i];
                }
                networkSettings.wifiMode = wiFiMode.toInt();

                request->send(200, TEXT_PLAN, OK_RESPONSE);

                settingsManager->getNavigator()->saveNetworkSettingsAndRestart(&networkSettings);

            }
        } else {
            String message = "Read operation for \"wifi\" block unsupported. Use read for every parameter";
            LOGGER.error(message);
            request->send(200, TEXT_PLAN, message);
        }
    } else {
        if (operation == PARAMETER_OPERATION_READ) {
            String message = settingsManager->getNavigator()->getSettingByName(parameter);
            request->send(200, TEXT_PLAN, message);
        } else {
            // Запись параметров

            //  Подсчет кол-ва параметров
            int paramsCount = 1;
            int indexOfParamSeparator = -1;
            while ((indexOfParamSeparator = parameter.indexOf('|', indexOfParamSeparator + 1)) >= 0)
                paramsCount++;

            String params[paramsCount];

            // Соберем массивы
            indexOfParamSeparator = parameter.indexOf('|');
            int startCopyIndex = 0;
            int paramIndex = 0;
            if (parameter.indexOf('|') >= 0)
                do {
                    params[paramIndex++] = indexOfParamSeparator == -1
                                           ? parameter.substring(startCopyIndex)
                                           : parameter.substring(startCopyIndex, indexOfParamSeparator);
                    startCopyIndex = indexOfParamSeparator + 1;
                } while ((indexOfParamSeparator = parameter.indexOf('|', indexOfParamSeparator + 1)) >= 0);
            // последний элемент
            if (startCopyIndex < parameter.length()) {
                params[paramIndex] = indexOfParamSeparator == -1
                                     ? parameter.substring(startCopyIndex)
                                     : parameter.substring(startCopyIndex, indexOfParamSeparator);
            }

            for (int i = 0; i < paramsCount; LOGGER.debug(params[i++]));

            String message = settingsManager->getNavigator()->saveSettingsByNames(&params[0], paramsCount);

            if (message.isEmpty()) {
                request->send(200, TEXT_PLAN, OK_RESPONSE);
            } else {
                request->send(503, TEXT_PLAN, message);
                LOGGER.error(message);
            }

        }
    }

}
//----------------------------------------------------------------------

void WebServerController::profilesApiProcessor(AsyncWebServerRequest *request) {
    String operation = request->arg(PROFILES_OPERATION_ATTR_NAME);
    String result = "";
    bool errorOccurred = false;

    if (operation.isEmpty()) {
        request->send(502, TEXT_PLAN, "operation can't be null");
    } else {
        if (operation == PROFILES_OPERATION_GET_LIST) {
            result = settingsManager->getNavigator()->getUserProfilesList();
        } else if (operation == PROFILES_OPERATION_ACTIVATE) {
            String profileName = request->arg(PROFILES_NAME_ATTR_NAME);
            if (profileName.isEmpty()) {
                result = "ProfileName can't be empty";
                errorOccurred = true;
            } else {
                // найдем профиль по индексу
                if (settingsManager->getNavigator()->activateProfile(profileName)) {
                    result = "OK";
                } else {
                    result = "Profile with name \"" + profileName + "\" not found.";
                    errorOccurred = true;
                }
            }
        } else if (operation == PROFILES_OPERATION_SAVE) {
            String profileName = request->arg(PROFILES_NAME_ATTR_NAME);
            if (profileName.isEmpty()) {
                result = "ProfileName can't be empty";
                errorOccurred = true;
            } else {
                result = settingsManager->getNavigator()->saveProfile(profileName);
                errorOccurred = !result.isEmpty();
                if (!errorOccurred) {
                    result = "OK";
                }
            }
        } else if (operation == PROFILES_OPERATION_REMOVE) {
            result = settingsManager->getNavigator()->removeProfile();
            errorOccurred = !result.isEmpty();
            if (!errorOccurred) {
                result = "OK";
            }
        }

        if (result.isEmpty()) {
            request->send(502, TEXT_PLAN, "unknown operation \"" + operation + "\"");
        } else if (errorOccurred) {
            request->send(502, TEXT_PLAN, result);
        } else {
            request->send(200, TEXT_JSON, result);
        }
    }
}
//----------------------------------------------------------------------

void WebServerController::loadFileByUrl(AsyncWebServerRequest *request) {
    String url = request->url();
    String mime;

    File testFile = SPIFFS.open(request->url(), "r");
    if (!testFile) {

        LOGGER.error("url not found: \"" + request->url() + "\"");

        request->send(404, TEXT_PLAN, "not found");
    } else {
        testFile.close();

        if (url.endsWith(".html")) {
            mime = "text/html";
        } else if (url.endsWith(".css")) {
            mime = "text/css";
        } else if (url.endsWith(".js")) {
            mime = "text/js";
        } else if (url.endsWith(".jpg")) {
            mime = "image/jpeg";
        } else {
            mime = TEXT_PLAN;
        }

        request->send(SPIFFS, url, mime);
    }
}
//----------------------------------------------------------------------


String WebServerController::systemSettingsProcessor(const String &paramName) {
    return settingsManager->getNavigator()->getSettingByName(paramName);
}
//----------------------------------------------------------------------
