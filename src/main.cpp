#include "../../../../../.platformio/packages/framework-arduinoespressif32/cores/esp32/HardwareSerial.h"
#include "WiFiController.h"
#include "../../../../../.platformio/packages/framework-arduinoespressif32/cores/esp32/WString.h"
#include "../../../../../.platformio/packages/framework-arduinoespressif32/libraries/SPIFFS/src/SPIFFS.h"
#include "../../../../../.platformio/packages/framework-arduinoespressif32/cores/esp32/Esp.h"
#include "../../../../../.platformio/packages/framework-arduinoespressif32/tools/sdk/include/heap/esp_heap_caps.h"
#include "../../../../../.platformio/packages/framework-arduinoespressif32/tools/sdk/include/lwip/lwipopts.h"
#include "Logger.h"
#include "../../../../../.platformio/packages/framework-arduinoespressif32/libraries/ArduinoOTA/src/ArduinoOTA.h"
#include "../../../../../.platformio/packages/framework-arduinoespressif32/tools/sdk/include/soc/soc/gpio_struct.h"
#include "EFlame.h"
#include "AvgBrightSweepFlameEffect.h"
#include "HotAgeFlameEffect.h"
#include "NormalizeFlameFlameEffect.h"
#include "TrembleFlameEffect.h"
#include "../../../../../.platformio/packages/framework-arduinoespressif32/variants/esp32/pins_arduino.h"
#include "../../../../../.platformio/packages/framework-arduinoespressif32/cores/esp32/Arduino.h"


EFlame *flame;
long lastWork;


SettingsManager *settingsManager;
WiFiController *wiFiController;

void detectResetButton() {

    if (digitalRead(34) == 0) {
        long waitStartedAt = millis();
        while (millis() - waitStartedAt < 3000){
            if (digitalRead(34)){
                return;
            }
        }

        // alarm button pressed
        LOGGER.warning("Alarm button pressed");
        delay(100);

        while (digitalRead(34) == 0) {
            for (int ledIndex = 0; ledIndex < 4; ledcWrite(ledIndex++, PWM_MAX_WIDTH));
            delay(30);
            for (int ledIndex = 0; ledIndex < 4; ledcWrite(ledIndex++, 0));
            delay(30);
        }

        settingsManager->resetWiFi();
        settingsManager->readSettings(&settingsManager->getNavigator()->tempSettings);
        memcpy(&settingsManager->getNavigator()->tempSettings.network, &settingsManager->getSettings()->network,
               sizeof(NetworkSettings));
        settingsManager->saveSetting(&settingsManager->getNavigator()->tempSettings, true);
    }


}


void setup() {
#ifdef CON_DEBUG
    Serial.begin(115200);
    Serial.println("---");

    LOGGER.info("Started UART at 921600");
#endif
    LOGGER.info("Starting...");

    SPIFFS.begin(true);

    pinMode(34, INPUT);

    settingsManager = new SettingsManager();
    settingsManager->firePaused = false;


    wiFiController = new WiFiController(settingsManager);

    ArduinoOTA.begin();

    flame = new EFlame(settingsManager);

    detectResetButton();

    AbstractFlameEffect *effect;

    effect = new AvgBrightChangeFlameEffect(flame);
    flame->appendEffect(effect);
    effect = new HotAgeFlameEffect(flame);
    flame->appendEffect(effect);
    effect = new NormalizeFlameFlameEffect(flame);
    flame->appendEffect(effect);

    effect = new TrembleFlameEffect(flame);
    flame->appendEffect(effect);

    lastWork = millis();

//    initNewMatrix();
}

void loop() {
    if ((millis() - lastWork) > settingsManager->getSettings()->activeProfile.flameSettings.loopIntervalMs) {
        lastWork = millis();

        if (settingsManager->firePaused) {
            for (int i = 0; i < 4; i++) {
                ledcWrite(i, settingsManager->pinValues[i]);
            }
        } else {
            flame->loop();
        }

//        onTimer();

        detectResetButton();
    }

    wiFiController->handle();
    ArduinoOTA.handle();


}