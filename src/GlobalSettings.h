#ifndef EFLAME328_GLOBALSETTINGS_H
#define EFLAME328_GLOBALSETTINGS_H

#define GLOBAL_CURRENT_SETTINGS_VERSION 1
#define GLOBAL_SETTINGS_MARKER_0 0x32
#define GLOBAL_SETTINGS_MARKER_1 0x32
#define GLOBAL_SETTINGS_MARKER_2 0x33
#define GLOBAL_SETTINGS_MARKER_3 0x33

#define MAX_PROFILE_COUNTER 10
#define SYSTEM_PROFILE_COUNTER 5


struct TrembleSettings {
    /**
     * Матрица коэффициентов для смены "горячей зоны"
     */
    float matrix[8][4];

    /**
     * Минимальное значение изменения яркости. Проценты
     */
    float minBrightnessChangePrct;

    /**
     * Максимальное значение изменения яркости. Проценты
     */
    float maxBrightnessChangePrct;

    /**
     * Минимальное кол-во шагов
     */
    int minStepCount;

    /**
     * Максимальное кол-во шагов
     */
    int maxStepCount;

    /**
     * Вероятность возникновения именно этого эффекта из всех. Вес
     */
    float probabilityCoef;
};


struct AvgBrightSweepSettings {
    /**
     * Максимальное уменьшение яркости в процентах
     */
    float maxFadeDownPrct;

    /**
     * Максимальное увеличение яркости в процентах
     */
    float maxFadeUpPrct;

    /**
    * Вероятность возникновения именно этого эффекта из всех. Вес
    */
    float probabilityCoef;

};

struct HotAgeSettings {
    /**
     * Минимальное значение увеличения яркости. Проценты
     */
    float minBrightnessChangePrct;

    /**
     * Максимальное значение увеличения яркости. Проценты
     */
    float maxBrightnessChangePrct;

    /**
     * Минимальное кол-во шагов
     */
    int minStepCount;

    /**
     * Максимальное кол-во шагов
     */
    int maxStepCount;

    /**
     * Вероятность возникновения именно этого эффекта из всех. Вес
     */
    float probabilityCoef;
};

struct NormalizeSettings {
    /**
     * Минимальное кол-во шагов
     */
    int minStepCount;

    /**
     * Максимальное кол-во шагов
     */
    int maxStepCount;

    /**
     * Вероятность возникновения именно этого эффекта из всех. Вес
     */
    float probabilityCoef;
};

struct FlameSettings {
    /**
   * Интервал между циклами обработки пламени
   */
    int loopIntervalMs;

    /**
     * Выключить через СЕКУНД после включения. 0 = не выключать
     */
    int shutdownAfterSec;

    /**
     * Яркость в процентах от расчетного. Это коэффициент используется при расчете PWM
     */
    float brightnessCoefficientPrct;

    /**
     * Нормальная средняя яркость пламени. От этого значения и будут считаться все отклонения яркости вверх или вниз и
     * к нему будет стремиться вернуться яркость. Проценты
     */
    float centerBrightnessPrct;

    /**
     * Коэффициент вероятности возникновения нового эффекта. в 100 циклов
     */
    float newEffectProbabilityPer100CyclesPrct;

    /**
     * Вероятность возникновения нового эффекта, пока не завершился предыдущий
     */
    float probabilityChangeActionPrct;

};

/**
 * Профиль настроек
 */
struct Profile {
    /**
     * Пользовательское название
     */
    char name[94];

    /**
     * Признак, что слот свободен
     */
    bool isFree;

    /**
     * Признак, что профиль встроенный. Его нельзя переименовывать, менять или удалять
     */
    bool system;


    /**
     * Настройки эффектов
     */
    FlameSettings flameSettings;
    NormalizeSettings normalizeSettings;
    HotAgeSettings hotAgeSettings;
    AvgBrightSweepSettings avgBrightSweepSettings;
    TrembleSettings trembleSettings;
};

struct NetworkSettings {
    /**
  * Настройки WiFi
  */
    char ssid[64];
    char password[64];
    unsigned char ipAddress[4];

    /**
     * Имя станции светильника в сети
     */
    char hostName[64];

    /**
     * режим работы сети .  WiFiMode
     */
    unsigned char wifiMode;

    /**
     * Резерв сюда будут идти вставки новых настроек, чтобы не сбить имеющиеся настройки при расширении
     */
    char reserved[64];

};

struct GlobalSettings {
    /**
     * Признак, что настройки записаны, а не пустое пространство. Маркеом является определенныя последовательность
    */
    char initMarker[4];

    /**
     * Версия настроек
    */
    unsigned char version;

    /**
     * Сетевые настройки и настройки WiFi
     */
    NetworkSettings network;

    /**
     * Настройки, с которыми работают все эффекты и основной механизм. При выборе пользователем другого профиля его
     * данные будут копироваться в этот. Из него же они будут переноситься в библиотеку профилей пользователя
     */
    Profile activeProfile;

    /**
     * Системные и пользовательские профили
     */
    Profile userProfiles[MAX_PROFILE_COUNTER];


    /**
     * Общая яркость
     */
    float globalBrightnessPrct;
};

#endif //EFLAME328_SETTINGS_H

