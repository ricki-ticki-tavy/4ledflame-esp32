//
// Created by dsporykhin on 29.02.20.
//

#include "EFlame.h"
#include "Defines.h"
#include "Logger.h"
#include "SettingsManager.h"
#include "../../../../../.platformio/packages/framework-arduinoespressif32/cores/esp32/esp32-hal-gpio.h"
#include "../../../../../.platformio/packages/framework-arduinoespressif32/cores/esp32/Arduino.h"

EFlame::EFlame(SettingsManager *settingsManager) {

    this->settingsManager = settingsManager;

    effectCounter = 0;
    stepCountToDo = 0;
    centerBrightnessPrct = settingsManager->getSettings()->activeProfile.flameSettings.centerBrightnessPrct;
    priorCenterBrightnessPrctInSettings = centerBrightnessPrct;

    Serial.println("Start config leds...");

    for (int index = 0; index < 4; index++) {
        brightDelta[index] = 0;
        brights[index] = 0;
        ledcAttachPin(ledMatrix[index], index);
        double freq = ledcSetup(index, 1500, 15);
        Serial.println("channel " + String(index) + " set at " + String(freq) + " Hz");
    };

    shutdownTimerActive = false;
    shutdownInProgress = false;

    Serial.println("Leds configured.");
}
//---------------------------------------------------------------------------

double EFlame::doubleRandom() {
    long maxVal = 1200000000L;
    long val = random(maxVal);
    double d = val;
    return d / (double) (maxVal);
}
//---------------------------------------------------------------------------

void EFlame::appendEffect(AbstractFlameEffect *effect) {
    if (effectCounter < MAX_EFFECTS_COUNT) {
        effects[effectCounter++] = effect;
    }
}
//---------------------------------------------------------------------------

double EFlame::doubleAbs(double src) {
    return src < 0 ? -src : src;
}
//---------------------------------------------------------------------------

void EFlame::hdSetLedBright(int index) {
    double coef = settingsManager->getSettings()->activeProfile.flameSettings.brightnessCoefficientPrct / (double) 100;
    coef *= settingsManager->getSettings()->globalBrightnessPrct / (double) 100;
    ledcWrite(index, brights[index] * coef);
}
//---------------------------------------------------------------------------

float EFlame::getTotalEffectsProbability() {
    double totalEffectProbability = 0;
    for (int effectIndex = 0;
         effectIndex < effectCounter; totalEffectProbability += effects[effectIndex++]->getProbabilityCoef());
    return totalEffectProbability;
}
//---------------------------------------------------------------------------

void EFlame::applyNewEffect() {
//    Serial.println("start looking for new effect");


    double totalEffectProbability = getTotalEffectsProbability();

    // берем некое ранодомное число, не превышающее суммарной вероянтности выпадения всех эффектов.
    double effectSelector = doubleRandom() * totalEffectProbability;
    int newEffectIndex = -1;

    // Все вероятности сложены, как бы, в единый столбик. Выпавшее число не юольше размера этого столбика. ищем в кого
    // в этом столбике попало наше число. Чем выше было значение вероятности для эффекта, тем с большей вероятностью
    // число попадет именно в его диапазон
    for (int index = 0; index < effectCounter; index++) {
        if (effects[index]->getProbabilityCoef() > effectSelector) {
            newEffectIndex = index;
            break;
        } else {
            effectSelector -= effects[index]->getProbabilityCoef();
        }
    }

    if (newEffectIndex != -1) {
        // все нормально. Иначе и быть не должно, но проверка должна быть
        effects[newEffectIndex]->apply();
    }
//    Serial.println("effect selected");
}
//---------------------------------------------------------------------------

void EFlame::loop() {
    // Проверка на то, что в настройках была изменен центр яркости
    if (settingsManager->getSettings()->activeProfile.flameSettings.centerBrightnessPrct !=
        priorCenterBrightnessPrctInSettings) {
        centerBrightnessPrct = settingsManager->getSettings()->activeProfile.flameSettings.centerBrightnessPrct;
        priorCenterBrightnessPrctInSettings = centerBrightnessPrct;
    }

    double rand = doubleRandom();
    if (stepCountToDo <= 0 ||
        rand <= settingsManager->getSettings()->activeProfile.flameSettings.probabilityChangeActionPrct) {
        // меняем направление свечения и все такое
        if (stepCountToDo > 0    // именно так! если он больше, но мы тут, то сработала безусловная смена эффекта
            || doubleRandom() <= getTotalEffectsProbability()
                                 *
                                 settingsManager->getSettings()->activeProfile.flameSettings.newEffectProbabilityPer100CyclesPrct
                                 / 100) {
            if (stepCountToDo > 0) {
            }
            applyNewEffect();
        }
    } else {
        for (int index = 0; index < 4; index++) {
            brights[index] += brightDelta[index];
            if (brights[index] < 0) {
                brights[index] = 0;
            } else if (brights[index] > PWM_MAX_WIDTH) {
                brights[index] = PWM_MAX_WIDTH;
            }
            hdSetLedBright(index);
        }
        stepCountToDo--;
    }

    // check if need to activate the shutdown timer
    if (settingsManager->getSettings()->activeProfile.flameSettings.shutdownAfterSec && !shutdownTimerActive){
        // the timer is not active, but countdown timer hes been set so we need ti activate shutdown timer
        Serial.println("shutdownTimer activated");
        shutdownTimerActive = true;
        shutdownInProgress = false;
        shutdownTimerStartedAt = millis();
        shutdownTimerOrigin = settingsManager->getSettings()->activeProfile.flameSettings.shutdownAfterSec;
    } else if (shutdownTimerActive && !settingsManager->getSettings()->activeProfile.flameSettings.shutdownAfterSec){
        Serial.println("shutdownTimer reseted");
        // the shutdown timer has an active state, but countdown timer has been reset by user. So we need to stop shutdown timer
        shutdownTimerActive = false;
        shutdownTimerOrigin = 0;
        shutdownInProgress = false;
    }

    // handling of shutdown timer if it's on
    if (shutdownTimerActive) {
        // the shutdown timer is active
        settingsManager->getSettings()->activeProfile.flameSettings.shutdownAfterSec =
                shutdownTimerOrigin - (millis() - shutdownTimerStartedAt) / 1000;

        if (settingsManager->getSettings()->activeProfile.flameSettings.shutdownAfterSec <= 0){
            // the time is up. Lets shut down the light
            Serial.println("shutdownTimer is up");
            settingsManager->getSettings()->activeProfile.flameSettings.shutdownAfterSec = 0;
            shutdownTimerActive = false;
            // включаем режим плавного гашения яркости
            shutdownTimerOrigin = settingsManager->getSettings()->globalBrightnessPrct;
            shutdownTimerStartedAt = millis();
            shutdownInProgress = true;
        }
    } else if (shutdownInProgress) {
        // mode to turning off is active
        settingsManager->getSettings()->globalBrightnessPrct = shutdownTimerOrigin - (millis() - shutdownTimerStartedAt) / 300;
        if (settingsManager->getSettings()->globalBrightnessPrct <= 0){
            Serial.println("shutdownTimer is off");
            // thr light is off
            settingsManager->getSettings()->globalBrightnessPrct = 0;
            shutdownInProgress = false;
        }
    }
}
//---------------------------------------------------------------------------

SettingsManager *EFlame::getSettingsManager() {
    return settingsManager;
}
//---------------------------------------------------------------------------

double EFlame::getCurrentNormalPrightPrct() {
    return centerBrightnessPrct;
}
//---------------------------------------------------------------------------

double EFlame::getAbsCurrentNormalBright() {
    return centerBrightnessPrct * (double)PWM_MAX_WIDTH / (double) 100;
}
//---------------------------------------------------------------------------

void EFlame::setCurrentNormalPrightPrct(double value) {
    centerBrightnessPrct = value;
}
//---------------------------------------------------------------------------
