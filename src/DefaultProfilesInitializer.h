//
// Created by dsporykhin on 24.04.20.
//

#ifndef EFLAMEESP8266_DEFAULTPROFILESINITIALIZER_H
#define EFLAMEESP8266_DEFAULTPROFILESINITIALIZER_H


#include "GlobalSettings.h"

class DefaultProfilesInitializer {
public:
    /**
     * Инициализация системных профилей
     * @param userProfiles
     */
    static void initSystemProfiles(Profile* userProfiles);
};


#endif //EFLAMEESP8266_DEFAULTPROFILESINITIALIZER_H
