//
// Created by dsporykhin on 01.03.20.
// tremble effect
//

#ifndef EFLAME328_TREMBLEFLAMEEFFECT_H
#define EFLAME328_TREMBLEFLAMEEFFECT_H


#include "AbstractFlameEffect.h"

class TrembleFlameEffect: public AbstractFlameEffect {
public:
    TrembleFlameEffect(EFlame* eFlame);

    float getProbabilityCoef();

    void applyEffect();

};


#endif //EFLAME328_TREMBLEFLAMEEFFECT_H
