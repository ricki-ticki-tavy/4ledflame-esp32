//
// Created by dsporykhin on 01.03.20.
//

#include <stdlib.h>
#include "NormalizeFlameFlameEffect.h"
#include "Defines.h"
#include "Logger.h"
#include "../../../../../.platformio/packages/framework-arduinoespressif32/cores/esp32/Arduino.h"

NormalizeFlameFlameEffect::NormalizeFlameFlameEffect(EFlame *eflame) : AbstractFlameEffect(eflame, true) {

}

void NormalizeFlameFlameEffect::applyEffect() {
    flame->stepCountToDo = random(flame->getSettingsManager()->getSettings()->activeProfile.normalizeSettings.minStepCount
            , flame->getSettingsManager()->getSettings()->activeProfile.normalizeSettings.maxStepCount + 1);

    for (int index = 0; index < 4; index++) {
        flame->brightDelta[index] =
                (flame->getAbsCurrentNormalBright() - flame->brights[index]) / (double) flame->stepCountToDo;
    }
}

float NormalizeFlameFlameEffect::getProbabilityCoef(){
    return flame->getSettingsManager()->getSettings()->activeProfile.normalizeSettings.probabilityCoef;
}
