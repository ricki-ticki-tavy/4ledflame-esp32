#include "../../../../../.platformio/packages/framework-arduinoespressif32/cores/esp32/HardwareSerial.h"
#include "WiFiController.h"
#include "../../../../../.platformio/packages/framework-arduinoespressif32/cores/esp32/WString.h"
#include "../../../../../.platformio/packages/framework-arduinoespressif32/libraries/SPIFFS/src/SPIFFS.h"
#include "../../../../../.platformio/packages/framework-arduinoespressif32/cores/esp32/Esp.h"
#include "../../../../../.platformio/packages/framework-arduinoespressif32/tools/sdk/include/heap/esp_heap_caps.h"
#include "../../../../../.platformio/packages/framework-arduinoespressif32/tools/sdk/include/lwip/lwipopts.h"

long startedAt;
WiFiController* wiFiController;
SettingsManager *settingsManager;
String data = "123";

void setup() {

//    Serial.begin(921600);
    Serial.begin(115200);

    Serial.println("init SPIFFS");
    SPIFFS.begin(true);
    Serial.println("init SPIFFS");

    startedAt = millis();
    settingsManager = new SettingsManager();

    wiFiController = new WiFiController(settingsManager);
}

void loop() {
    if (millis() - startedAt > 1000) {
        startedAt = millis();
        Serial.println(String(startedAt));
        data = String(startedAt);
        if (data.isEmpty()) {

        }
        Serial.println(String(esp_get_minimum_free_heap_size()));


    }
}