//
// Created by dsporykhin on 07.05.20.
//

#ifndef TEST2ESP32_WEBSERVERCONTROLLER_H
#define TEST2ESP32_WEBSERVERCONTROLLER_H


#include "../../../../../.platformio/packages/framework-arduinoespressif32/tools/sdk/include/esp_http_server/http_server.h"
#include "../../../../../.platformio/packages/framework-arduinoespressif32/tools/sdk/include/nghttp/http_parser.h"
#include "../../../../../.platformio/packages/framework-arduinoespressif32/tools/sdk/include/esp32/esp_err.h"

class WebServerController {
private:

    static esp_err_t get_handler(httpd_req_t *req);
    static esp_err_t post_handler(httpd_req_t *req);

    httpd_handle_t server = NULL;

    httpd_uri_t uri_get = {
            .uri      = "*",
            .method   = HTTP_GET,
            .handler  = get_handler,
            .user_ctx = NULL
    };

/* URI handler structure for POST /uri */
    httpd_uri_t uri_post = {
            .uri      = "/uri",
            .method   = HTTP_POST,
            .handler  = post_handler,
            .user_ctx = NULL
    };


public:
    WebServerController();

};


#endif //TEST2ESP32_WEBSERVERCONTROLLER_H
