//
// Created by dsporykhin on 07.05.20.
//

#include "WebServerController.h"
#include "../../../../../.platformio/packages/framework-arduinoespressif32/tools/sdk/include/esp_http_server/http_server.h"
#include "../../../../../.platformio/packages/framework-arduinoespressif32/tools/sdk/include/freertos/freertos/task.h"
#include "../../../../../.platformio/packages/framework-arduinoespressif32/tools/sdk/include/esp32/esp_err.h"
#include "../../../../../.platformio/packages/framework-arduinoespressif32/cores/esp32/HardwareSerial.h"

WebServerController::WebServerController() {
    httpd_config_t config = HTTPD_DEFAULT_CONFIG();

    if (httpd_start(&server, &config) == ESP_OK) {
        /* Register URI handlers */
        httpd_register_uri_handler(server, &uri_get);
        httpd_register_uri_handler(server, &uri_post);
    }
}

esp_err_t WebServerController::get_handler(httpd_req_t *req) {
    Serial.println("request incoming. Url \"" + String(req->uri) + "\"");
    const char resp[] = "URI GET Response";
    httpd_resp_send(req, resp, strlen(resp));
    return ESP_OK;
}

esp_err_t WebServerController::post_handler(httpd_req_t *req) {
    return ESP_OK;
}